package com.yedam.poly;

public class CarExample {
	public static void main(String[] args) {
		Car car = new Car();

		for (int i = 0; i <= 5; i++) {
			int problemLoc = car.run();

			switch (problemLoc) {
			//frontLeftTire
			case 1:
				System.out.println("앞 왼쪽 HankookTire 교환");
				//Tire = 부모클래스(슈퍼클래스)
				//HanKookTire = 자식클래스(서브클래스)
				//Tire frontLeftTire = new HanKookTire("앞왼쪽", 15);
				car.frontLeftTire = new HanKookTire("앞왼쪽", 15);
				break;
			case 2:
				System.out.println("앞 오른쪽 KumhoTire 교환");
				car.frontRightTire = new KumhoTire("앞오른쪽", 15);
				break;
			case 3:
				System.out.println("뒤왼쪽 HankookTire 교환");
				car.backLeftTire = new HanKookTire("왼 뒤쪽", 15);
				break;
			case 4:
				System.out.println("뒤 오른쪽 KumhoTire 교환");
				car.backRightTire = new KumhoTire("왼오른쪽",15);
				break;
			}
			System.out.println("====================================");
		}
	}
}
