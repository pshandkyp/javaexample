package com.yedam.poly;

public class ChildExample {
	public static void main(String[] args) {
		//Child child = new Child();
		
		//☆★클래스간의 자동타입변환☆★
		//부모 클래스에 있는 메소드를 사용하되
		//단, 자식클래스에 재정의가 되어 있으면
		//자식 클래스에 재정의된 메소드를 사용하겠습니다.
		//Parent parent = new Child();
		
		//parent.method1();
		//parent.method2();
		//parent.method3();
		
		// 클래스간의 강제 타입 변환
		// 자동타입변환으로 인해서 자식클래스 내부에 정의된 필드, 메소드를 못 쓸 경우
		// 강제타입변환을 함으로써 자식 클래스 내부에 정의된 필드와 메소드를 사용.
		Parent parent = new Child();
		
		parent.field = "data1";
		parent.method1();
		parent.method2();
//		parent.field2 = "data2";
//		parent.method3();
		
		Child child = (Child) parent;
		child.field2 = "data2";
		child.method3();
		child.method1();
		child.method2();
		child.field = "data";
		
		//클래스 타입 확인 예제
		
		method1(new Parent());
		method1(new Child());
		
		GrandParent gp = new Child();
		gp.method4();
		
	}
	
	public static void method1(Parent parent) {
		if(parent instanceof Child) {
			Child child = (Child) parent;
			System.out.println("변환 성공");
		} else {
			System.out.println("변환 실패");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
