package com.yedam.poly;

public class DrawExample {
	public static void main(String[] args) {
		//자동타입변환
		//부모클래스 변수 = new 자식클래스();
		
		// 다형성을 구현
		Draw figure = new Circle();
		
		figure.x = 1;
		figure.y = 2;
		
		figure.draw();
		
		figure = new Rectangle();
		
		figure.draw();
		
		
		
	}
}
