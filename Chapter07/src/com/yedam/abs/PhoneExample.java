package com.yedam.abs;

public class PhoneExample {

	public static void main(String[] args) {
		// 추상 클래스 객체(인스턴스)화 확인
		// Phone phone = new Phone("Owner");
		SmartPhone smartPhone = new SmartPhone("홍길동");
		
		smartPhone.turnOn();
		smartPhone.turnOff();
		
		smartPhone.internetSearch();

	}
}
