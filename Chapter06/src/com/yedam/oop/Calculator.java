package com.yedam.oop;

public class Calculator {
	
	//필드
	//정적 필드
	static double pi = 3.14;
	//생성자
	
	//메소드
	//정적 메소드
	static int plus(int x, int y) {
		return x+y;
	}
	
	
	int sum(int a, int b) {
		result("메소드 연습");
		return a+b;
	}
	//매개변수의 데이터 타입 차이에 따른 오버로딩
	// int -> double
	double sum (double a, double b) {
		return a+b;
	}
	//매개변수의 갯수에 따른 오버로딩
	//매개변수가 2->1
	int sum(int a) {
		return a;
	}
	//매개변수의 데이터 타입 차이에 따른 오버로딩
	//int, int -> double, int
	double sum(double a, int b) {
		return a+b;
	}
	//매개변수의 순서 차이에 따른 오버로딩
	//double, int -> int, double
	double sum(int b, double a) {
		return a+b;
	}
	
	
	
	double sub(int a, int b) {
		return a-b;
	}
	
//	String result(String value) {
//		String temp = "value return 테스트 : " +value;
//		return temp;
//	}
	
	void result(String value) {
		System.out.println("value return 테스트 : " +value);
	}

	
	
	
	
	
	
	
	
}
