package com.yedam.oop;

import java.util.Scanner;

public class Homework {
	public static void main(String[] args) {
		// 문제2) 다음은 키보드로부터 상품 수와 상품 가격을 입력받아서
		//->Scanner 상품 수, 가격
		// Scanner sc = new Scanner(System.in);
		
		// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
		
		// 1)최고 가격 -> Max값 찾기 , 해당 제품을 제외한 제품들의 총 합 = 모든 제품 의 합 - 최고 가격
		//Product pd = null;
		// 2-1) 최고 가격의 제품 찾을 때 인덱스 또는 값을 따로 저장 해놓고
		//for(int i = 0; i.pd.length; i++){
		//	최대값 구하는 로직
		//  제품의 총합 구하기
		//}
		// 2-2) 반복문을 한번 더 돌려서 해당 제품을 제외하고(조건문) 합을 한다.
		
		// 1) 메뉴는 다음과 같이 구성하세요.
		// 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료
		
		// while(){
		// 메뉴 출력
		// 메뉴을 진입할 수 있도록 조건문 통해서 구별을 해 주면 된다.
		// }
		
		// 2) 입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
		// 배열써야한다 -> Scanner 사용해서 배열의 크기를 할당.
		// 상품 클래스를 만들 때 필드로는 상품명과 가격이 들어 가야한다.
		// 입력 -> 반복문을 활용(배열의 크기만큼 반복)
		
		// 3) 제품별 가격을 출력하세요.
		// 출력예시, "상품명 : 가격"
		// 출력 -> 반복문을 활용(배열의 크기만큼) -> 각 방에 있는 객체를 하나씩 꺼내옴.
		// 객체가 가지고 있는 필드(정보를 담은 변수) -> 출력 예시에 맞게 만든다.
		
		// 4) 분석기능은 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총합을 구합니다.
		
		// 5) 종료 시에는 프로그램을 종료한다고 메세지를 출력하도록 구현하세요.
		// 반복문을 종료하기 직전에 프로그램 종료 한다는 System.out.println() 출력하면 됨.
		
		//if
		
		//상품의 수와 상품 명을 입력 받을 수 있도록 Scanner 등록
		Scanner sc = new Scanner(System.in);
		Product[] pd = null;
		int productCount = 0;
		//boolean flag = true;
		while(true) {
			
			System.out.println("1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료");
			System.out.println("입력>");
			
			String selectNo = sc.nextLine();
			//1번 , 상품 수
			if(selectNo.equals("1")) {
				System.out.println("상품 수 입력>");
				productCount = Integer.parseInt(sc.nextLine());
			} else if(selectNo.equals("2")) {
				//상품 수 입력받은 내용을 토대로 배열 크기를 확정
				pd = new Product[productCount];
				//prodcutCount = 5
				//pd.length = 5
				for(int i=0; i<pd.length; i++) {
					//상품 객체 생성
					//반복문 실행할때 마다 새로운 상품을 만들기 위해서
					//아래 내용을 정의.
					Product product = new Product();
					System.out.println((i+1)+"번째 상품");
					System.out.println("상품명>");
					//변수에 데이터를 입력 받고 객체에 데이터를 넣는 방법
					String name = sc.nextLine();
					product.ProductName = name;
					
					System.out.println("가격>");
					//데이터를 입력 받음과 동시에 객체에 데이터를 넣는 방법
					product.price = Integer.parseInt(sc.nextLine());
										
					pd[i] = product;
					System.out.println("=============");
				}
			} else if(selectNo.equals("3")) {
				//배열의 크기만 반복문을 진행할때 배열에 각 인덱스(방 번호)를 
				//활용하여 객체(상품)을 꺼내와서 객체(상품)에 정보를 하나씩 꺼내옴.
				for(int i = 0; i<pd.length; i++) {
					//상품 명
					String name = pd[i].ProductName;
					//String name = product.ProductName;
					//상품 가격
					//int price = pd[i].price;
					
					System.out.println("상품 명 : " + name);
					//System.out.println("상품 가격 : " + price);
					System.out.println("상품 가격 : " + pd[i].price);
				}
			} else if(selectNo.equals("4")) {
				// 1)최고 가격 -> Max값 찾기 , 해당 제품을 제외한 제품들의 총 합 = 모든 제품 의 합 - 최고 가격
				// 2-1) 최고 가격의 제품 찾을 때 인덱스 또는 값을 따로 저장 해놓고
				//for(int i = 0; i.pd.length; i++){
				//	최대값 구하는 로직
				//  제품의 총합 구하기
				//}
				// 2-2) 반복문을 한번 더 돌려서 해당 제품을 제외하고(조건문) 합을 한다.
				int max = pd[0].price;
				int sum = 0;
				for(int i =0; i<pd.length; i++) {
					//제일 비싼 상품 가격 구하기
					if(max < pd[i].price) {
						max = pd[i].price;
					}
					//상품들의 가격 합계 구하기
					sum += pd[i].price;
				}
				System.out.println("제일 비싼 상품 가격 : " + max);
				System.out.println("제일 비싼 상품을 제외한 상품 총 합 : " + (sum-max));
			} else if(selectNo.equals("5")) {
				System.out.println("프로그램 종료");
				break;
			}
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
