package com.yedam.oop;

public class Application {
	public static void main(String[] args) {
		//SmartPhone 클래스(설계도)를 토대로 iphone14Pro
		SmartPhone iphone14Pro = new SmartPhone("iphone14Pro", 500);
//		iphone14Pro.maker ="Apple";
//		iphone14Pro.name = "iphone14Pro";
//		iphone14Pro.price = 100000;
//		
//		iphone14Pro.price = 500;
		
		iphone14Pro.call();
		iphone14Pro.hangUp();
		
		//필드 정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);

		//SmartPhone 클래스(설계도)
		
//		SmartPhone zfilp4 = new SmartPhone();
//		zfilp4.maker = "samsung";
//		zfilp4.name = "zfilp4";
//		zfilp4.price = 10000;
//		
//		zfilp4.call();
//		zfilp4.hangUp();
//		
//		//System.out.println(iphone14Pro.maker);
		
		SmartPhone sony = new SmartPhone();
		
		//리턴 타입이 없는 메소드
		sony.getInfo(0);
		//리턴 타입이 int인 메소드
		int b = sony.getInfo("int");
		//리턴 타입이 String[]인 메소드
		String[] temp = sony.getInfo(args);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
