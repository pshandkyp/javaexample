package com.yedam.oop;

public class Book {
	/*
	 * 책 이름 : 혼자 공부하는 자바
	 * # 내용 
	 * 1) 종류 : 학습서
	 * 2) 가격 : 24000원
	 * 3) 출판사 : 한빛미디어
	 * 4) 도서번호 : yedam-001
	 * 
	 */
	//필드
	String bookName;
	String kind = "학습서";
	int price;
	String publisher;
	String isbn;
	//생성자
	public Book(String bookName, int price, String publisher, String isbn) {
		this.bookName = bookName;
		this.price = price;
		this.publisher = publisher;
		this.isbn = isbn;		
	}
	//메소드
	void getInfo() {
		System.out.println("책 이름 : " + bookName);
		System.out.println("종류 : " + kind);
		System.out.println("가격 : " + price);
		System.out.println("출판사 : " + publisher);
		System.out.println("도서번호 : " + isbn);
	}
	
	
	}
