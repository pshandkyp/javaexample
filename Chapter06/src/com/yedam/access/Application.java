package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free = "free";
		
		//protected
		access.parent = "parent";
		
		//private
//		access.privacy = "privacy";
		
		//default
		access.basic = "basic";
		
		access.free();
		//access.privacy();
		
		Singleton obj1 = Singleton.getInstance();
		Singleton obj2 = Singleton.getInstance();
		//객체간 주소 비교는 ==
		if(obj1 == obj2) {
			System.out.println("같은 싱글톤 객체입니다.");
		}else {
			System.out.println("다른 싱글톤 객체입니다.");
		}
		
		
		
		
		
		
		
		
	}
}
