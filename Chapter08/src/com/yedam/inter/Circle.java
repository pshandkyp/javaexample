package com.yedam.inter;

public class Circle implements GetInfo {

	// 필드
	int radius;

	// 생성자
	public Circle(int radius) {
		this.radius = radius;
	}
	// 메소드

	@Override
	public void aera() {
		//원 넓이 PI * R * R
		System.out.println(3.14 * radius * radius);
	}

	@Override
	public void round() {
		// 원둘레 2 * PI * R
		System.out.println(2 * 3.14 * radius);
	}

}
