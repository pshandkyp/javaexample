package com.yedam.inter;

public class WSExample {
	public static void main(String[] args) {
		WashingMachine LGws = new LGWashingMachine();
		LGws.startBtn();
		LGws.pauseBtn();
		System.out.println("세탁기 속도 : " + LGws.changeSpeed(2)
		+ " 변경하였습니다.");
		LGws.stopBtn();
		
		LGws.dry();
	}
}
