package com.yedam.inter2;

public class Car {
	Tire frontLeftTire = new HankookTire();
	Tire frontRightTire = new KumhoTire();
	Tire backLeftTire = new HankookTire();
	Tire backRigthTire = new KumhoTire();
	
	public void run() {
		frontLeftTire.roll();
		frontRightTire.roll();
		backLeftTire.roll();
		backRigthTire.roll();
	}
}
