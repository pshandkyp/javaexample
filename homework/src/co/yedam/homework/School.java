package co.yedam.homework;

import java.util.Scanner;

public class School {
	// 학생 관리 프로그램 구현.
	// 관리 해야 할 학생 수 입력 후 학생 수만큼 정보 입력.
	// 학생 정보 : 학번, 이름, 국어, 수학, 영어
	// 학생 번호 입력 시, 학생의 기본 정보 및 평균 출력.
	// getter, setter 이용해서 데이터 입력

	// 추가 문제, 학생 번호 입력시, 추가적으로 평균으로 등수 출력 기능.
	// 조건 1) class를 이용하여 학생을 등록.
	// 조건 2) class에서 학생의 기본 정보 및 평균을 출력하는 메소드 사용.

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		Student[] students = null;
		
		while (true) {
			System.out.println("============================================================");
			System.out.println("1. 학생수 | 2. 학생 정보 입력 | 3. 학생 정보 출력 | 4. 순위 | 5. 종료");
			System.out.println("============================================================");
			System.out.println("선택>");
			int menu = scn.nextInt();
		
			switch(menu) {
			case 1:
				System.out.println("학생 수>");
				int num = scn.nextInt();
				students = new Student[num]; //객체배열
				break;
			case 2:
				for(int i=0; i<students.length; i++) {
					Student std = new Student();
					
					System.out.println("학번>");
					int no = scn.nextInt();
					std.setStudentNo(no);
					
					System.out.println("이름>");
					String name = scn.next();
					std.setStudentName(name);
					
					System.out.println("국어>");
					int kor = scn.nextInt();
					std.setKor(kor);
					
					System.out.println("영어>");
					int eng = scn.nextInt();
					std.setEng(eng);
					
					System.out.println("수학>");
					int math = scn.nextInt();
					std.setMath(math);
					System.out.println("===========================");
					
//					Student std = new Student(no, name, kor, eng, math);
					students[i] = std;
				}
				break;
			case 3:
				System.out.println("학번 입력>");
				int no = scn.nextInt();
				for(int i=0; i<students.length; i++) {
					if(students[i].getStudentNo() == no) {
						System.out.println(students[i].toString());
					}
				}
				break;
			case 4:
				System.out.println("순위>");
				for(int i=0; i<students.length-1; i++) {
					for(int j=0; j<students.length-1-i; j++) {
						if(students[i].studAvg() > students[i+1].studAvg()) {
							Student temp = students[i];
							students[i] = students[i+1];
							students[i+1] = temp;
						}
					}
				}
				for(int i=0; i<students.length; i++) {
					System.out.println(i+1 + "등>" + students[i].getStudentName() + ", 평균>" + String.format("%.1f", students[i].studAvg()));
				}
				break;
				
			case 5:
				System.out.println("프로그램을 종료합니다.");
				System.exit(0);
			}
		}
	}
}