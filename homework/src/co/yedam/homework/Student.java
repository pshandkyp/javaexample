package co.yedam.homework;

import java.util.ArrayList;
import java.util.List;

public class Student {
	// 필드
	// 학생 정보 : 학번, 이름, 국어, 수학, 영어
	private int studentNo;
	private String studentName;
	private int kor;
	private int eng;
	private int math;

	// 생성자
	public Student() {
		super();
	}
	
	public Student(int studentNo, String studentName, int kor, int eng, int math) {
		super();
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.kor = kor;
		this.eng = eng;
		this.math = math;
	}

	// 메소드
	public int getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(int studentNo) {
		this.studentNo = studentNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getKor() {
		return kor;
	}

	public void setKor(int kor) {
		this.kor = kor;
	}

	public int getEng() {
		return eng;
	}

	public void setEng(int eng) {
		this.eng = eng;
	}

	public int getMath() {
		return math;
	}

	public void setMath(int math) {
		this.math = math;
	}

	//평균구하는 함수 
	public float studAvg() {
		float avg = (kor+eng+math)/3;
		return avg;
	}
	
	@Override
	public String toString() {
		String info = "이름>" + studentName 
				+ "\n국어>" + kor 
				+ "\n영어>" + eng
				+ "\n수학>" + math
				+ "\n평균>" + String.format("%.1f", studAvg());
		return info;
	}
}
