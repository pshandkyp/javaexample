package homework2;
//- Employee 클래스를 상속한다
public class EmpDept extends Employee{
//	- 추가로 부서이름을 필드로 가지며 
	public String deptName;
	
//- Employee 클래스를 상속한다.
//생성자를 이용하여 값을 초기화한다.
	public EmpDept(String name, String salary, String deptName) {
		super(name, salary); //부모 클래스 객체 생성
		this.deptName = deptName;
	}
	
//	- 추가된 필드의 getter를 정의한다.
	public String getDeptName() {
		return deptName;
	}
//	(1) public void getInformation() : 이름과 연봉, 부서를 출력하는 기능
	@Override
	public void getInformation() {
		super.getInformation();
		System.out.println(" 부서 : " + deptName);
	}
//	(2) public void print() : "수퍼클래스\n서브클래스"란 문구를 출력하는 기능
	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}

	
	
	
	
	
	
	
	
	
	
}
