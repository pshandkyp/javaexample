package homework2;

//- Human 클래스를 상속한다.
public class StandardWeightInfo extends Human{

	public StandardWeightInfo(String name, double height, double weight) {
		super(name, height, weight);
	}

//	- 메소드는 다음과 같이 정의한다.
//	(1) public void getInformation() : 이름, 키, 몸무게와 표준체중을 출력하는 기능
	@Override
	public void getInformation() {
		super.getInformation();
		//1번 방식
		//System.out.println("표준체중 : " + getStandardWeight());
		//2번 방식
		System.out.printf("표준체중 %.1f 입니다.\n", getStandardWeight());
	}
//	(2) public double getStandardWeight() : 표준체중을 구하는 기능
//	( * 표준 체중 : (Height - 100) * 0.9 )
	
	public double getStandardWeight() {
		double sw = (height - 100) * 0.9;
		return sw;
	}
	
	
	
	
	
	
	
	
	
}
