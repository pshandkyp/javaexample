package com.yedam.java.feedback;

import java.util.Scanner;

public class Feedback03 {

	public static void main(String[] args) {
		// 추가문제2) 입력받은 학생 정보를 이용하여 각 등급을 배정하는 프로그램을 만드세요.
		//			예를 들어 전체 학생의 상위 10%(A), 20%(B), 30%(C), 40%(D)
		// 입력 값 : Elena(65), Suzie(74), john(23), Emily(75), Neda(68),
		//          Kate(96), Alex(88), Daniel(98), Hamilton(54)
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("입력받을 학생 수>");
		int size = Integer.parseInt(scanner.nextLine());
		
		String[] names = new String[size];
		int[] scores = new int[size];
		
		for(int i=0; i<size; i++) {
			String str = scanner.nextLine();
			String[] temp = str.split(" ");
			names[i] = temp[0];
			scores[i] = Integer.parseInt(temp[1]);
		}
		
		for(int i=0; i<scores.length; i++) {
			for(int j=i; j<scores.length; j++) {
				if(scores[i] < scores[j]) {
					int scoresTemp = scores[i];
					scores[i] = scores[j];
					scores[j] = scoresTemp;
					
					String nameTemp = names[i];
					names[i] = names[j];
					names[j] = nameTemp;
				}
			}
		}
		
		for(int i=0; i<scores.length; i++) {
			double ratio = (double)(i+1) / scores.length * 100;

			if(ratio <= 10) {
				System.out.printf("%s %d A\n", names[i], scores[i]);
			}else if(ratio <=30) {
				System.out.printf("%s %d B\n", names[i], scores[i]);
			}else if(ratio <= 60) {
				System.out.printf("%s %d C\n", names[i], scores[i]);
			}else {
				System.out.printf("%s %d D\n", names[i], scores[i]);
			}
		}
		
	}

}
