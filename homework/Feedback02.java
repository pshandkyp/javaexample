package com.yedam.java.feedback;

import java.util.Scanner;

public class Feedback02 {

	public static void main(String[] args) {
		// 추가문제1) A,E,I,O,U를 모음이라고 가정했을 때,
		// 입력으로 들어온 문자열이 몇개의 모음과 자음으로 구성되어 있는지
		// 출력하는 프로그램을 작성하세요.
		// 입력 예시 -> 입력할 문자열의 개수 : 2
		//			  Programming is fun
		//			  Hello World
		// 출력 예시 -> Programming is fun : 5 11
		//			  Hello World : 3 7
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("입력할 문자열의 개수>");
		int size = Integer.parseInt(scanner.nextLine());
		String[] list = new String[size];
		for(int i=0; i<list.length; i++) {
			list[i] = scanner.nextLine();
		}
		
		for(String str : list) {
			int[] temp = new int[2];
			for(int i=0; i<str.length(); i++) {
				char ch = str.charAt(i); 
				switch(ch) {
				case ' ':
					break;
				case 'A':
				case 'a':
				case 'E':
				case 'e':
				case 'I':
				case 'i':
				case 'O':
				case 'o':
				case 'U':
				case 'u':
					temp[0]++;
					break;
				default :
					temp[1]++;
					break;
				}
			}
			System.out.printf("%s : %d %d\n", str, temp[0], temp[1]);
		}
		
	}
}
